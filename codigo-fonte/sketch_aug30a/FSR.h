#ifndef fsr_h
#define fsr_h

#include "Wire.h"   
#include "MPU9250.h"
#include "MadgwickAHRS.h"
#include "math.h"

#define N 10 

class FSR
{
  public:
  FSR(int fsrPin_sensor1); // armazena a leitura do pino 26
  /*FSR(int fsrPin_sensor2); // armazena a leitura do pino 26
  FSR(int fsrPin_sensor3); // armazena a leitura do pino 26
  FSR(int fsrPin_sensor4); // armazena a leitura do pino 26
  FSR(int fsrPin_sensor5); // armazena a leitura do pino 26*/
                            
  int fsrVoltage1;                          // leitura do pino convertido em voltage
  int fsrVoltage2;                          // leitura do pino convertido em voltage
  int fsrVoltage3;                          // leitura do pino convertido em voltage
  int fsrVoltage4;                          // leitura do pino convertido em voltage
  int fsrVoltage5;                          // leitura do pino convertido em voltage
  
  int fsrReading1;                           // armazena a leitura do pino 26
  int fsrReading2;                           // armazena a leitura do pino 26
  int fsrReading3;                           // armazena a leitura do pino 26
  int fsrReading4;                           // armazena a leitura do pino 26
  int fsrReading5;                           // armazena a leitura do pino 26
  
  unsigned long fsrResistance1;            // voltage convertida para resistência do sensor
  unsigned long fsrResistance2;            // voltage convertida para resistência do sensor
  unsigned long fsrResistance3;            // voltage convertida para resistência do sensor
  unsigned long fsrResistance4;            // voltage convertida para resistência do sensor
  unsigned long fsrResistance5;            // voltage convertida para resistência do sensor
  
  unsigned long fsrConductance1; 
  unsigned long fsrConductance2; 
  unsigned long fsrConductance3; 
  unsigned long fsrConductance4;
  unsigned long fsrConductance5; 

  unsigned long peso1;                    // resistência convertida para força do sensor 1
  unsigned long peso2;                    // resistência convertida para força do sensor 1
  unsigned long peso3;                    // resistência convertida para força do sensor 1
  unsigned long peso4;                    // resistência convertida para força do sensor 1
  unsigned long peso5;                    // resistência convertida para força do sensor 1
  
  unsigned long aux_peso1;
  float aux_peso11;
  int firstRun1= 1;

  long fsrForce1;                          // resistência convertida para força  // resistência convertida para força
  long fsrForce2;                          // resistência convertida para força  // resistência convertida para força
  long fsrForce3;                          // resistência convertida para força  // resistência convertida para força
  long fsrForce4;                          // resistência convertida para força  // resistência convertida para força
  long fsrForce5;                          // resistência convertida para força  // resistência convertida para força
  
  int rodada= 1;
  int Vout_original1;                      //recebe valor do pino 26
  int Vout_original2;                      //recebe valor do pino 26
  int Vout_original3;                      //recebe valor do pino 26
  int Vout_original4;                      //recebe valor do pino 26
  int Vout_original5;                      //recebe valor do pino 26
  
  int Vout_filtrado1;                      // recebe o valor já filtrado
  int * numbers_sensor1 = new int[N];                         // vetor com os valores para media movel
  int * numbers_sensor2 = new int[N];
  int * numbers_sensor3 = new int[N];
  int * numbers_sensor4 = new int[N];
  int * numbers_sensor5 = new int[N];
  int cont_numbers = 0;
  int media_exp_ant=0;
  int media_exp=0;
  float gravidade = 9.8;                  // valor da gravidade
  
  int calibration_sensor1(int Vout_original1);
  int calibration_sensor2(int Vout_original2);
  int calibration_sensor3(int Vout_original3);
  int calibration_sensor4(int Vout_original4);
  int calibration_sensor5(int Vout_original5);
  
  void moving_average(int Vout_original1, int Vout_original2, int Vout_original3, int Vout_original4, int Vout_original5, long int Vout_all_filter[]);

  int calc_average(int numbers[]);
  int * shift_numbers(int numbers[], int new_vout_original);
};

#endif
