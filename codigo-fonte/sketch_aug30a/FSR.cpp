
#include "FSR.h"
#include "math.h"
FSR::FSR(int fsrPin_sensor1){ //, int fsrPin_sensor2, int fsrPin_sensor3, int fsrPin_sensor4, int fsrPin_sensor5){
    
  Vout_original1 = fsrPin_sensor1;
}; //construtor

int FSR::calibration_sensor1(int Vout_filtrado1)
{
  //Serial.println("Dados Sensor 1: ");
  int fsrReading1 = Vout_filtrado1;
 
  // analog voltage reading ranges from about 0 to 4095 which maps to 0V to 3.3V (= 3300mV)
  fsrVoltage1 = map(fsrReading1, 0, 4095, 0, 3300);   
 
  if (fsrVoltage1 == 0) {
   // Serial.println("No pressure");  
  } else {
     //The voltage = Vcc * R / (R + FSR) where R = 3.3K and Vcc = 3.3V
    // so FSR = ((Vcc - V) * R) / V        yay math!
    fsrResistance1 = 3300 - fsrVoltage1;     // fsrVoltage is in millivolts so 3.3V = 3300mV // entra media
    fsrResistance1 *= 3300;                // 3.3K resistor
    fsrResistance1 /= fsrVoltage1;

    fsrConductance1 = 1000000;           // we measure in micromhos so 
    fsrConductance1 /= fsrResistance1;
    
    // Calculo do P=f(Voltagem) sensor 1
    peso1 = (0.0003*fsrVoltage1*fsrVoltage1)+(0.2628*fsrVoltage1)+(0.0572); // Peso = 0,0003Vout^2+0,2628Vout+0,057

//     Achando a força em newton
    fsrForce1 = peso1*gravidade;
    return fsrForce1;
    /*
    // Use the two FSR guide graphs to approximate the force
    if (fsrConductance <= 1000) {
      fsrForce = fsrConductance / 80;
      Serial.print("Force in Newtons: ");
      Serial.println(fsrForce);      
    } else {
      fsrForce = fsrConductance - 1000;
      fsrForce /= 30;
      Serial.print("Force in Newtons: ");
      Serial.println(fsrForce);            
    }
    */
  }
}

int FSR::calibration_sensor2(int Vout_filtrado2)
{
  //Serial.println("Dados Sensor 2: ");
  int fsrReading2 = Vout_filtrado2; //// colocar valor filtrado em fsrReading 
 // Serial.print("Analog reading = ");
 // Serial.println(fsrReading1);
 
  // analog voltage reading ranges from about 0 to 4095 which maps to 0V to 3.3V (= 3300mV)
  fsrVoltage2 = map(fsrReading2, 0, 4095, 0, 3300);
  
  if (fsrVoltage2 == 0) {  
  } else {
    fsrResistance2 = 3300 - fsrVoltage2;     // fsrVoltage is in millivolts so 3.3V = 3300mV // entra media
    fsrResistance2 *= 3300;                // 3.3K resistor
    fsrResistance2 /= fsrVoltage2;
    
    // Calculo do P=f(Voltagem) sensor 1
    peso2 = (0.0075*fsrVoltage2*fsrVoltage2)+(0.4368*fsrVoltage2)+(0.00562); 

//     Achando a força em newton
    fsrForce2 = peso2*gravidade;
    return fsrForce2;          
    }
  }


int FSR::calibration_sensor3(int Vout_filtrado3)
{
  //Serial.println("Dados Sensor 3: ");
  int fsrReading3 = Vout_filtrado3; //// colocar valor filtrado em fsrReading 
 
  // analog voltage reading ranges from about 0 to 4095 which maps to 0V to 3.3V (= 3300mV)
  fsrVoltage3 = map(fsrReading3, 0, 4095, 0, 3300);
  
  if (fsrVoltage3 == 0) {  
  } else {
    fsrResistance3 = 3300 - fsrVoltage3;     // fsrVoltage is in millivolts so 3.3V = 3300mV // entra media
    fsrResistance3 *= 3300;                // 3.3K resistor
    fsrResistance3 /= fsrVoltage3;
    
    // Calculo do P=f(Voltagem) sensor 3
    peso3 = (0.0075*fsrVoltage3*fsrVoltage3)+(0.4368*fsrVoltage3)+(0.00562); 

//     Achando a força em newton
    fsrForce3 = peso3*gravidade;
    return fsrForce3;          
    }
  }


int FSR::calibration_sensor4(int Vout_filtrado4)
{
  //Serial.println("Dados Sensor 4: ");
  int fsrReading4 = Vout_filtrado4; //// colocar valor filtrado em fsrReading 
 
  // analog voltage reading ranges from about 0 to 4095 which maps to 0V to 3.3V (= 3300mV)
  fsrVoltage4 = map(fsrReading4, 0, 4095, 0, 3300);
  
  if (fsrVoltage4 == 0) {  
  } else {
    fsrResistance4 = 3300 - fsrVoltage4;     // fsrVoltage is in millivolts so 3.3V = 3300mV // entra media
    fsrResistance4 *= 3300;                // 3.3K resistor
    fsrResistance4 /= fsrVoltage4;
    
    // Calculo do P=f(Voltagem) sensor 4
    peso4 = (0.0075*fsrVoltage4*fsrVoltage4)+(0.4368*fsrVoltage4)+(0.00562); 

//     Achando a força em newton
    fsrForce4 = peso4*gravidade;
    return fsrForce4;          
    }
  }


int FSR::calibration_sensor5(int Vout_filtrado5)
{
  //Serial.println("Dados Sensor 5: ");
  int fsrReading5 = Vout_filtrado5; //// colocar valor filtrado em fsrReading 
 
  // analog voltage reading ranges from about 0 to 4095 which maps to 0V to 3.3V (= 3300mV)
  fsrVoltage5 = map(fsrReading5, 0, 4095, 0, 3300);
  
  if (fsrVoltage5 == 0) {  
  } else {
    fsrResistance5 = 3300 - fsrVoltage5;     // fsrVoltage is in millivolts so 3.3V = 3300mV // entra media
    fsrResistance5 *= 3300;                // 3.3K resistor
    fsrResistance5 /= fsrVoltage5;
    
    // Calculo do P=f(Voltagem) sensor 4
    peso5 = (0.0075*fsrVoltage5*fsrVoltage5)+(0.4368*fsrVoltage5)+(0.00562); 

//     Achando a força em newton
    fsrForce5 = peso5*gravidade;
    return fsrForce5;          
    }
  }

int FSR::calc_average(int numbers[]) {
    int soma = 0;
  
    for(int i = 0; i < cont_numbers; i++) {
        soma += numbers[i];
    }

    return soma / cont_numbers;
}

int * FSR::shift_numbers(int numbers[], int new_vout_original) {
    for(int i = 0; i < (cont_numbers - 1); i++) {
        numbers[i] = numbers[i + 1]; 
    }

    numbers[9] = new_vout_original;

    return numbers;
}

void FSR::moving_average(int Vout_original1, int Vout_original2, int Vout_original3, int Vout_original4, int Vout_original5, long int Vout_all_filter[])
{
    if(cont_numbers <= 9) {
        numbers_sensor1[cont_numbers] = Vout_original1;
        numbers_sensor2[cont_numbers] = Vout_original2;
        numbers_sensor3[cont_numbers] = Vout_original3;
        numbers_sensor4[cont_numbers] = Vout_original4;
        numbers_sensor5[cont_numbers] = Vout_original5;
        cont_numbers++;
    } else {
        numbers_sensor1 = shift_numbers(numbers_sensor1, Vout_original1);
        numbers_sensor2 = shift_numbers(numbers_sensor2, Vout_original2);
        numbers_sensor3 = shift_numbers(numbers_sensor3, Vout_original3);
        numbers_sensor4 = shift_numbers(numbers_sensor4, Vout_original4);
        numbers_sensor5 = shift_numbers(numbers_sensor5, Vout_original5);
    }

    Vout_all_filter[0] = calc_average(numbers_sensor1);
    Vout_all_filter[1] = calc_average(numbers_sensor2);
    Vout_all_filter[2] = calc_average(numbers_sensor3);
    Vout_all_filter[3] = calc_average(numbers_sensor4);
    Vout_all_filter[4] = calc_average(numbers_sensor5);

    //Serial.println("Sensor 1");
//    for(int i = 0; i < cont_numbers; i++) {
//      Serial.print(numbers_sensor1[i]);
//      Serial.print(", ");
//    }
//    Serial.println();
//    Serial.println("Sensor 2");
//    for(int i = 0; i < cont_numbers; i++) {
//      Serial.print(numbers_sensor2[i]);
//      Serial.print(", ");
//    }
//    Serial.println();
//    Serial.println("Sensor 3");
//    for(int i = 0; i < cont_numbers; i++) {
//      Serial.print(numbers_sensor3[i]);
//      Serial.print(", ");
//    }
//    Serial.println();
//    Serial.println("Sensor 4");
//    for(int i = 0; i < cont_numbers; i++) {
//      Serial.print(numbers_sensor4[i]);
//      Serial.print(", ");
//    }
//    Serial.println();
//    Serial.println("Sensor 5");
//    for(int i = 0; i < cont_numbers; i++) {
//      Serial.print(numbers_sensor1[i]);
//      Serial.print(", ");
//    }

//  long acc =0;                    // somador dos pontos da media movel
//  int janela1;
//  
//    if (firstRun1){
//      janela1 =10;
//      int matriz1[janela1];
//      numbers[janela1]=Vout_original1*(matriz1[janela1]);
//      firstRun1=false;
//    }
//  
//    //desloca os elementos do vetor de media movel
//    for(int i=janela1-1; i>0; i--){
//      numbers[i] = numbers[i-1];
//    }
//    numbers[0]= Vout_original1;      // posicao inicial do vetor recebe leitura original
//
//    for(int i=0; i<janela1; i++){
//    acc += numbers[i];          // faz somatoria de numero de pontos
//    }
//  return acc/janela1;
}
