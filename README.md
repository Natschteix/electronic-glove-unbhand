# Electronic Glove - UnbHand
|Matrícula    | Aluno                              | GitLab                                                     |
| ----------  | ---------------------------------- | ---------------------------------------------------------- |
| 18/0042971  | Natália Schulz Teixeira         | [@Natschteix](https://gitlab.com/Natschteix)                  |

 Luva eletrônica para controle de mão robótica é um projeto que utiliza sensores FSR (Force-Sensing Resistor) na ponta dos dedos e MPUs (Unidades de Medição Inercial) nas falanges. Seu objetivo é classificar movimentos de agarre por meio do algoritmo simpleLogistic, implementado no software Weka, que retorna várias equações de primeiro grau.

<div align="center">
    <img src="./documents/imagens/luva_frente.jpeg"/>    
    <figcaption>Figura 01. Imagem da luva pela frente.</figcaption>

</div>

<div align="center">
    <img src="./documents/imagens/luva_costas.jpeg"/>    
    <figcaption>Figura 02. Imagem da luva pelas costas.</figcaption>

</div>


Os sensores FSR na ponta dos dedos medem a força aplicada em cada dedo durante o movimento de agarre. Esses sensores fornecem dados sobre a pressão exercida, permitindo uma avaliação da intensidade do movimento.

As MPUs nas falanges dos dedos registram informações sobre aceleração, orientação e velocidade angular. Esses dados são essenciais para determinar a posição e a direção do movimento realizado por cada dedo.

Utilizando os dados coletados pelos sensores FSR e MPUs, o algoritmo simpleLogistic, implementado no software Weka, é empregado para classificar os movimentos de agarre executados pelo usuário. Esse algoritmo é capaz de aprender padrões a partir dos dados de entrada e retornar equações de primeiro grau que descrevem a relação entre as variáveis de entrada e a classificação do movimento.

A luva eletrônica possibilita um controle preciso e intuitivo da mão robótica, permitindo uma interação natural entre o usuário e o robô. Ao classificar os movimentos de agarre, a mão robótica pode reproduzir de forma precisa e em tempo real os movimentos desejados.

O algoritmo simpleLogistic, ao retornar várias equações de primeiro grau, oferece uma visão mais detalhada das relações entre as variáveis de entrada e a classificação do movimento. Isso pode auxiliar na compreensão dos fatores que influenciam os movimentos de agarre e no aprimoramento do sistema de controle da mão robótica.

Esse projeto de luva eletrônica, com sensores FSR de força na ponta dos dedos, MPUs nas falanges e o algoritmo simpleLogistic do software Weka, apresenta um potencial significativo em diversas aplicações, como próteses de mão avançadas, robótica industrial e interação humano-robô. Seu desenvolvimento contribui para melhorar o controle e a precisão dos movimentos realizados pela mão robótica, beneficiando áreas como assistência médica, produção industrial e realidade virtual.

## Calibração do sensor de força FSR

Para calibração do sensor de força FSR, foi utilizado um setup construído para testar de forma a pegar toda a área do sensor com pesos de chumbinho.

Para isso foi feito um circuito de divisor de tensão conectado a um pino do microcontrolado, como indicado pelo datasheet da Interlink, utilizando um resistor de 10k.

<div align="center">
    <img src="./documents/imagens/circuito_calibracao.png"/>    
    <figcaption>Figura 03. circuito de calibração da FSR.</figcaption>

</div>

Fazendo isso com todos os sensores que foram posicionados nos 5 dedos obtemos os seguintes gráficos de calibração com suas respectivas equações.

<div align="center">
    <img src="./documents/imagens/graf_calibracao_dedao.png"/>    
    <figcaption>Figura 04. Gráfico de calibração do dedão.</figcaption>

</div>


<div align="center">
    <img src="./documents/imagens/graf_calibracao_indicador.png"/>    
    <figcaption>Figura 05. Gráfico de calibração do indicador.</figcaption>

</div>

<div align="center">
    <img src="./documents/imagens/graf_calibracao_dedomedio.png"/>    
    <figcaption>Figura 06. Gráfico de calibração do dedo médio.</figcaption>

</div>

## Classificação do dataset

<div align="center">
    <img src="./documents/imagens/acuracia.png"/>    
    <figcaption>Figura 07. Resultado da classificação.</figcaption>

</div>

<div align="center">
    <img src="./documents/imagens/matriz_confusao.png"/>    
    <figcaption>Figura 08. Matriz de Confusão.</figcaption>

</div>